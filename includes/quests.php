<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Quests extends Cache {

	protected $db;
	private $_xp_map = array();

	/**
	 * Initialize new search
	 */
	function __construct($db,$char=NULL) {
		$this->db = $db;
		$this->_map_xp();
	}
	
	/**
	 * Search in quests by quest titles
	 * @return array quests list
	 */
	public function search_by_title($string) {
		// consider adding cache here, but each keyword will produce different results that needs to be cached...

		// get all quests matching Title
		$get_quests = $this->db->query('
			SELECT qt.`Id`,qt.`Title`,qt.`Method`,qt.`Level`,qt.`MinLevel`,qt.`RequiredRaces`,qt.`ZoneOrSort`,qt.`Type`,qt.`RewardXpId`,qt.`RewardOrRequiredMoney`,qt.`RewardItemId1`,qt.`RewardItemId2`,qt.`RewardItemId3`,qt.`RewardItemId4`,qt.`RewardItemCount1`,qt.`RewardItemCount2`,qt.`RewardItemCount3`,qt.`RewardItemCount4`,qt.`RewardChoiceItemId1`,qt.`RewardChoiceItemId2`,qt.`RewardChoiceItemId3`,qt.`RewardChoiceItemId4`,qt.`RewardChoiceItemId5`,qt.`RewardChoiceItemId6`,qt.`RewardChoiceItemCount1`,qt.`RewardChoiceItemCount2`,qt.`RewardChoiceItemCount3`,qt.`RewardChoiceItemCount4`,qt.`RewardChoiceItemCount5`,qt.`RewardChoiceItemCount6`
			FROM `'.$this->db->worlddb.'`.`quest_template` AS qt
			WHERE qt.`Title` LIKE ? LIMIT '.SQL_LIMIT,	// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$string.'%')
		);

		return $this->_parse_results($get_quests->fetchAll(PDO::FETCH_ASSOC));
	}

	/**
	 * Search in quests by zone
	 * @return array quests list
	 */
	public function search_by_zone($zone) {

		if ($quests = $this->get_cache(array('zone_quests',$zone),GENERAL_DBC_EXPIRE)) {
			return $quests;
		}

		// get all quests matching zone
		$get_quests = $this->db->query('
			SELECT qt.`Id`,qt.`Title`,qt.`Method`,qt.`Level`,qt.`MinLevel`,qt.`RequiredRaces`,qt.`ZoneOrSort`,qt.`Type`,qt.`RewardXpId`,qt.`RewardOrRequiredMoney`,qt.`RewardItemId1`,qt.`RewardItemId2`,qt.`RewardItemId3`,qt.`RewardItemId4`,qt.`RewardItemCount1`,qt.`RewardItemCount2`,qt.`RewardItemCount3`,qt.`RewardItemCount4`,qt.`RewardChoiceItemId1`,qt.`RewardChoiceItemId2`,qt.`RewardChoiceItemId3`,qt.`RewardChoiceItemId4`,qt.`RewardChoiceItemId5`,qt.`RewardChoiceItemId6`,qt.`RewardChoiceItemCount1`,qt.`RewardChoiceItemCount2`,qt.`RewardChoiceItemCount3`,qt.`RewardChoiceItemCount4`,qt.`RewardChoiceItemCount5`,qt.`RewardChoiceItemCount6`
			FROM `'.$this->db->worlddb.'`.`quest_template` AS qt
			WHERE qt.`ZoneOrSort` = ?',		// no limitation
			array($zone)
		);

		$quests = $this->_parse_results($get_quests->fetchAll(PDO::FETCH_ASSOC));
		$this->store_cache(array('zone_quests',$zone),$quests);
		return $quests;
	}

	/**
	 * Search in quests by quest starter
	 * @return array quests list
	 */
	public function search_by_npc_starter($npc) {

		// no cache here - this is only helper and data are cached in Npc()

		// get all quests that are started by 
		$get_quests = $this->db->query('
			SELECT qt.`Id`,qt.`Title`,qt.`Method`,qt.`Level`,qt.`MinLevel`,qt.`RequiredRaces`,qt.`ZoneOrSort`,qt.`Type`,qt.`RewardXpId`,qt.`RewardOrRequiredMoney`,qt.`RewardItemId1`,qt.`RewardItemId2`,qt.`RewardItemId3`,qt.`RewardItemId4`,qt.`RewardItemCount1`,qt.`RewardItemCount2`,qt.`RewardItemCount3`,qt.`RewardItemCount4`,qt.`RewardChoiceItemId1`,qt.`RewardChoiceItemId2`,qt.`RewardChoiceItemId3`,qt.`RewardChoiceItemId4`,qt.`RewardChoiceItemId5`,qt.`RewardChoiceItemId6`,qt.`RewardChoiceItemCount1`,qt.`RewardChoiceItemCount2`,qt.`RewardChoiceItemCount3`,qt.`RewardChoiceItemCount4`,qt.`RewardChoiceItemCount5`,qt.`RewardChoiceItemCount6`
			FROM `'.$this->db->worlddb.'`.`creature_queststarter` AS cq
			LEFT JOIN `'.$this->db->worlddb.'`.`quest_template` AS qt ON (cq.`quest`=qt.`Id`)
			WHERE cq.`id` = ? LIMIT '.SQL_LIMIT,
			array($npc)
		);

		return $this->_parse_results($get_quests->fetchAll(PDO::FETCH_ASSOC));
	}

	/**
	 * Search in quests by quest ender
	 * @return array quests list
	 */
	public function search_by_npc_ender($npc) {

		// no cache here - this is only helper and data are cached in Npc()

		// get all quests that are started by 
		$get_quests = $this->db->query('
			SELECT qt.`Id`,qt.`Title`,qt.`Method`,qt.`Level`,qt.`MinLevel`,qt.`RequiredRaces`,qt.`ZoneOrSort`,qt.`Type`,qt.`RewardXpId`,qt.`RewardOrRequiredMoney`,qt.`RewardItemId1`,qt.`RewardItemId2`,qt.`RewardItemId3`,qt.`RewardItemId4`,qt.`RewardItemCount1`,qt.`RewardItemCount2`,qt.`RewardItemCount3`,qt.`RewardItemCount4`,qt.`RewardChoiceItemId1`,qt.`RewardChoiceItemId2`,qt.`RewardChoiceItemId3`,qt.`RewardChoiceItemId4`,qt.`RewardChoiceItemId5`,qt.`RewardChoiceItemId6`,qt.`RewardChoiceItemCount1`,qt.`RewardChoiceItemCount2`,qt.`RewardChoiceItemCount3`,qt.`RewardChoiceItemCount4`,qt.`RewardChoiceItemCount5`,qt.`RewardChoiceItemCount6`
			FROM `'.$this->db->worlddb.'`.`creature_questender` AS cq
			LEFT JOIN `'.$this->db->worlddb.'`.`quest_template` AS qt ON (cq.`quest`=qt.`Id`)
			WHERE cq.`id` = ? LIMIT '.SQL_LIMIT,
			array($npc)
		);

		return $this->_parse_results($get_quests->fetchAll(PDO::FETCH_ASSOC));
	}




	/**
	 * Parse quests results and apply some calculations and lookups
	 * @param array results
	 * @return array updated quests list
	 */
	private function _parse_results($results) {

		$quests = array();

		foreach ($results as $q) {
			// minlevel is used as reference. In game, there is diffFactor of QuestLevel and PlayerLevel
			// trinity core code below from QuestDef.cpp
			// uint32 Quest::XPValue(Player* player) const

			$diff_factor = 2 * ($q['Level'] - $q['MinLevel']) + 20;
			if ($diff_factor < 1)
				$diff_factor = 1;
			else if ($diff_factor > 10)
				$diff_factor = 10;

			$xp = 0;

			if ($q['MinLevel'] > 0) {
				$xp = $diff_factor * $this->xp_map[$q['MinLevel']]['col_'.($q['RewardXpId']+1)] / 10;
			}
		
			if ($xp == 0)
				$xp = 0;
			else if ($xp <= 100)
				$xp = 5 * (($xp + 2) / 5);
			else if ($xp <= 500)
				$xp = 10 * (($xp + 5) / 10);
			else if ($xp <= 1000)
				$xp = 25 * (($xp + 12) / 25);
			else
				$xp = 50 * (($xp + 25) / 50);
			// end of trinity code

			$reward_items = array();
			$choose_items = array();

			// lookup all items in rewards
			for ($i=1;$i<=4;++$i) {
				if ($q['RewardItemId'.$i] > 0) {
					$item = new Item($this->db);
					$item->get_by_entry($q['RewardItemId'.$i]);
					$rew_item = $item->get_item();
					$rew_item['count'] = $q['RewardItemCount'.$i];
					$reward_items[] = $rew_item;
				}
			}
			// lookup all choosable items in rewards
			for ($i=1;$i<=6;++$i) {
				if ($q['RewardChoiceItemId'.$i] > 0) {
					$item = new Item($this->db);
					$item->get_by_entry($q['RewardChoiceItemId'.$i]);
					$rew_item = $item->get_item();
					$rew_item['count'] = $q['RewardChoiceItemCount'.$i];
					$choose_items[] = $rew_item;
				}
			}

			$quests[] = array(
				'Id' => $q['Id'],
				'Title' => $q['Title'],
				'Method' => $q['Method'],
				'Level' => $q['Level'],
				'MinLevel' => $q['MinLevel'],
				'RequiredRaces' => $q['RequiredRaces'],
				'ZoneOrSort' => $q['ZoneOrSort'],
				'Type' => $q['Type'],
				'RewardOrRequiredMoney' => $q['RewardOrRequiredMoney'],
				'Experiences' => $xp,
				'RewardItems' => $reward_items,
				'ChooseItems' => $choose_items,
			);
		}
		return $quests;
	}

	/**
	 * Prefetch questxp table
	 */
	private function _map_xp() {
		// instead of requesting quest xp table for every result, lets fetch all 100 records and use them as they are needed.
		$this->xp_map = array();
		$get_quests_xp = $this->db->query('
			SELECT `col_0`,`col_1`,`col_2`,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`,`col_9`,`col_10`
			FROM `dbc_questxp`',
			array()
		);
		foreach ($get_quests_xp->fetchAll(PDO::FETCH_ASSOC) as $q) {
			$this->xp_map[$q['col_0']] = $q;
		}
	}

}
