<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Mapper extends Cache {

	protected $db;
	protected $_worldmap;

	public $map;

	/**
	 * Gets worldmap coordinates for each zone on defined map with optional polygon coords for zone
	 * @param PDO database handler
	 * @param integer map ID
	 */
	function __construct($db,$map) {
		$this->db = $db;
		$this->map = $map;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_worldmap = $this->get_cache(array('worldmaparea',$this->map),GENERAL_DBC_EXPIRE)) {
			return;
		}
	
		$get_areas = $this->db->query('
			SELECT wma.`col_2` AS zone,wma.`col_4` AS ya,wma.`col_5` AS yb,wma.`col_6` AS xa,wma.`col_7` AS xb,wm.coords
			FROM `dbc_worldmaparea` AS wma
			LEFT JOIN `worldmap` AS wm ON (wma.`col_2`=wm.`area_id`)
			WHERE wma.`col_1` = ?',
			array($map)
		);

		$this->_worldmap = $get_areas->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('worldmaparea',$this->map),$this->_worldmap);
	}

        /**
	 * Checks if spawn is inside one of zone from list of zonemaps. When zone is identified, it check if spawn lies inside the polygon coords.
         * @param float x spawn coordinates
         * @param float y spawn coordinates
         */
	public function check_spawn($x,$y) {

		$areas = array();
		foreach ($this->_worldmap as $w) {

			// test if in spawn lies inside map coords (lot of maps are overlapping)
			if ($x < $w['xa'] && $x > $w['xb'] && $y < $w['ya'] && $y > $w['yb']) {
				// zone identified, but spawns on zone borders obviously presents on multiple zones.
				// why have I feeling that x axis = y axis and vice versa?

				$cx = 515 / abs($w['xa']-$w['xb']);
				$cy = 772 / abs($w['ya']-$w['yb']);

				$nx = intval(($w['xa']-$x)*$cx);
				$ny = intval(($w['ya']-$y)*$cy);
								
				// check if spawn inside polygon (comes from mysql) to identify exact zone spawn belongs
				$is_in = $this->_is_in_polygon($w['coords'], $ny, $nx);

				if ($is_in) {
					$areas[] = array(intval($w['zone']), $ny, $nx);
				}
			}
		}
		return $areas;
	}

	/**
	 * Checks if spawn is inside of the polygon coords
	 * @param string polygon coordinates (space delimited twin-coords)
	 * @param float x spawn coordinates
	 * @param float y spawn coordinates
	 */

	private function _is_in_polygon($polygon, $x, $y) {
		if (!$polygon) {
			return false;
		}

		$coords = explode(' ',$polygon);
		$points_polygon = sizeof($coords) / 2;

		for ($i=0; $i<$points_polygon; ++$i) {
			$vx[$i] = $coords[($i*2)];
			$vy[$i] = $coords[($i*2+1)];
		}

		$i = $j = $c = 0;
		for ($i = 0, $j = $points_polygon-1 ; $i < $points_polygon; $j = $i++) {
			if ((($vy[$i] > $y != ($vy[$j] > $y)) && ($x < ($vx[$j] - $vx[$i]) * ($y - $vy[$i]) / ($vy[$j] - $vy[$i]) + $vx[$i]))) 
				$c = !$c;
		}
		return $c;
	}

}

