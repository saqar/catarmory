<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Achievements extends Cache {

	protected $db;

	/**
	* Initialize
	*/
	function __construct($db) {
		$this->db = $db;
	}

	/**
	 * Return character achievements categories list
	 * @return array of categories
	 */
	public function get_char_categories() {
		// search for cached data. Set variable and stop processing when found.
		if ($categories = $this->get_cache(array('achievement_char_categories',NULL),GENERAL_DBC_EXPIRE)) {
			return $categories;
		}

		$categories = $this->_get_categories(-1,array(1,15076));
		$this->store_cache(array('achievement_char_categories',NULL),$categories);
		return $categories;
	}	

	/**
	 * Return guild achievements categories list
	 * @return array of categories
	 */
	public function get_guild_categories() {
		// search for cached data. Set variable and stop processing when found.
		if ($categories = $this->get_cache(array('achievement_guild_categories',NULL),GENERAL_DBC_EXPIRE)) {
			return $categories;
		}

		$categories = $this->_get_categories(15076);
		$this->store_cache(array('achievement_guild_categories',NULL),$categories);
		return $categories;
	}


	/**
	 * Return achievements by name
	 * @return array of achievements
	 */
	public function search_by_name($name) {

		// get all achievements that match name
		$get_achievements = $this->db->query('
			SELECT da.`col_0` AS id,da.`col_1` AS faction,da.`col_2` AS map,da.`col_3` AS parent,da.`col_4` AS name,da.`col_5` AS description,da.`col_6` AS category_id,ac.`col_2` AS category,da.`col_7` AS points,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon,da.`col_11` AS reward,da.`col_12` AS count
			FROM `dbc_achievement` AS da
			LEFT JOIN `dbc_spellicon` AS dsi ON (da.`col_10`=dsi.col_0)
			LEFT JOIN `dbc_achievementcategory` AS ac ON (da.`col_6`=ac.col_0)
			WHERE da.`col_4` LIKE ? LIMIT '.SQL_LIMIT,
			array('%'.$name.'%')
		);
		$achievements = $get_achievements->fetchAll(PDO::FETCH_ASSOC);

		return $achievements;
	}

	/**
	 * Return achievements from category
	 * @return array of achievements
	 */
	public function search_by_category($category) {

		// search for cached data. Set variable and stop processing when found.
		if ($achievements = $this->get_cache(array('achievements_in_category',$category),GENERAL_DBC_EXPIRE)) {
			return $achievements;
		}

		// get all achievements matching zone
		$get_achievements = $this->db->query('
			SELECT da.`col_0` AS id,da.`col_1` AS faction,da.`col_2` AS map,da.`col_3` AS parent,da.`col_4` AS name,da.`col_5` AS description,da.`col_7` AS points,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon,da.`col_11` AS reward,da.`col_12` AS count
			FROM `dbc_achievement` AS da
			LEFT JOIN `dbc_spellicon` AS dsi ON (da.`col_10`=dsi.col_0)
			WHERE da.`col_6` = ? ORDER BY da.`col_8`',	// no limitation
			array($category)
		);

		$achievements = $get_achievements->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('achievements_in_category',$category),$achievements);

		return $achievements;
	}

	/**
	 * Perform nested lookup of categories
	 * @return array of arrays
	 */
	private function _get_categories($parent,$skip=array()) {
		$get_categories = $this->db->query('
			SELECT `col_0` AS id,`col_2` AS name
			FROM `dbc_achievementcategory`
			WHERE `col_1`=?
			ORDER BY `col_3`',
			array($parent)
		);

		$categories = array();
		foreach ($get_categories->fetchAll(PDO::FETCH_ASSOC) as $c) {
			if (!in_array($c['id'],$skip)) {
				$categories[] = array_merge((array)$c,array('childs' => $this->_get_categories($c['id'])));
			}
		}
		return $categories;
	}

}
