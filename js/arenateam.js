/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
 /**
 * Arenateam class
 * @class Arenateam
 * @singleton
 */
 
Arenateam = (function(a) {
	var Arenateam = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Arenateam.prototype, {
		/**
		 * Initialize Arenateam
		 * @param {Object} a Arenateam object data from server
		 */
		init: function(a) {
			this.teamId = a.arenaTeamId;
			this.name = a.arenateamName;
			this.type = a.type;
			this.faction = (1<<(a.captainRace-1) & racemaskAlliance ? 1 : 0);
			this.rating = a.rating;
			this.seasonGames = a.seasonGames;
			this.seasonWins = a.seasonWins;
			this.weekGames = a.weekGames;
			this.weekWins = a.weekWins;
			this.rank = a.rank;
			this.captainGuid = a.captainGuid;
			this.captainName = a.captainName;
			this.captainRace = a.captainRace;
			this.members = a.members;
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function() {
			$('#flag').css('background','url(\'images/b_'+this.faction+'.jpg\')');
			$('#arenateam_name').html(this.name);
			$('#arenateam_type').html(this.type+'v'+this.type);
			var $captain = $('<a  data-type="character" data-guid="'+this.captainGuid+'" href="character.html#'+this.captainName+'/basic">'+this.captainName+'</a>');
			$captain.tooltip();
			
			$('#arenateam_leader').html('Team captain: ');
			$('#arenateam_leader').append($captain);
			
			var list = new List(ListView.templates.arenateam,[{
				rating: this.rating,
				seasonGames: this.seasonGames,
				seasonWins: this.seasonWins,
				weekGames: this.weekGames,
				weekWins: this.weekWins,
				rank: this.rank,
				captainName: this.captainName,
				captainRace: this.captainRace
			}],{
				hideNumResults:true
			});
			list.render($('#arena_team_wrapper'));

			var membersList = new List(ListView.templates.arenateammembers,this.members,{
				hideNumResults:true
			});
			membersList.render($('#team_members_wrapper'));
		},

		/**
		 * Return Arenateam id
		 * @return {Number} id
		 */
		getTeamId: function() {
			return this.teamId;
		},
	});

	return Arenateam;
})();

