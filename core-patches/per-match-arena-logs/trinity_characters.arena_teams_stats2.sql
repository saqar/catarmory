DROP TABLE IF EXISTS `arena_teams_stats2`;

CREATE TABLE `arena_teams_stats2` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `instance_id` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        `arena_type` TINYINT(10) UNSIGNED NULL DEFAULT NULL,
        `arena_typeid` TINYINT(10) UNSIGNED NULL DEFAULT NULL,
        `start_time` DATETIME NULL DEFAULT NULL,
        `end_time` DATETIME NULL DEFAULT NULL,
        `team_winner` INT(10) UNSIGNED NULL DEFAULT NULL,
        `team_looser` INT(10) UNSIGNED NULL DEFAULT NULL,
        `rating_winner` INT(10) UNSIGNED NULL DEFAULT NULL,
        `rating_looser` INT(10) UNSIGNED NULL DEFAULT NULL,
        `change_winner` INT(10) NULL DEFAULT NULL,
        `change_looser` INT(10) NULL DEFAULT NULL,
        `healed_winner` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        `healed_looser` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        `damaged_winner` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        `damaged_looser` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        UNIQUE INDEX `id` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;
